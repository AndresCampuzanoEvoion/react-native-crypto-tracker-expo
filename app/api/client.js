import { create } from 'apisauce'

const apiClient = create({
  baseURL: 'https://api.coinlore.net/api'
})

export default apiClient
