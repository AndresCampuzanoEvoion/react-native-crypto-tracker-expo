import client from './client'

const endpoint = '/tickers/'

const getCoins = client.get(endpoint)

export default {
  getCoins
}
