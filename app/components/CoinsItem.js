import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import colors from '../config/colors'

const CoinsItem = ({
  name,
  symbol,
  price_usd,
  percent_change_24h,
  onPress
}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <View style={styles.containerText}>
        <Text style={styles.symbolStyle}>{symbol}</Text>
        <Text style={styles.nameStyle}>{name}</Text>
        <Text style={styles.priceStyle}>$ {price_usd}</Text>
      </View>
      <View style={styles.containerArrow}>
        <Text style={styles.changeStyle}>{percent_change_24h}</Text>
        {percent_change_24h >= 0 ? (
          <Image
            source={require('../assets/arrow_up.png')}
            style={styles.image}
          />
        ) : (
          <Image
            source={require('../assets/arrow_down.png')}
            style={styles.image}
          />
        )}
      </View>
    </TouchableOpacity>
  )
}

export default CoinsItem

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 16,
    backgroundColor: colors.charade
  },
  containerText: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  symbolStyle: {
    marginRight: 10,
    fontWeight: 'bold',
    color: colors.white
  },
  nameStyle: {
    marginRight: 10,
    color: colors.white
  },
  priceStyle: {
    color: colors.white
  },
  containerArrow: {
    flexDirection: 'row'
  },
  changeStyle: {
    marginRight: 10,
    color: colors.white
  },
  image: {
    width: 18,
    height: 18
  }
})
