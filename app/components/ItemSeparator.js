import React from 'react'
import { StyleSheet, View } from 'react-native'
import colors from '../config/colors'

const ItemSeparator = () => {
  return <View style={styles.container} />
}

export default ItemSeparator

const styles = StyleSheet.create({
  container: {
    height: 1,
    backgroundColor: colors.zircon
  }
})
