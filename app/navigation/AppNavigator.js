import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { MaterialCommunityIcons } from '@expo/vector-icons'

import FeedNavigator from './FeedNavigator'
import FavoritesNavigator from './FavoritesNavigator'
import colors from '../config/colors'

const Tab = createBottomTabNavigator()

const AppNavigator = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {
          backgroundColor: colors.blackPearl
        },
        activeBackgroundColor: colors.charade
      }}
    >
      <Tab.Screen
        name='Coins'
        component={FeedNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name='home' color={color} size={size} />
          )
        }}
      />
      <Tab.Screen
        name='Favorites'
        component={FavoritesNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name='home' color={color} size={size} />
          )
        }}
      />
    </Tab.Navigator>
  )
}

export default AppNavigator
