import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import FavoritesScreen from '../screens/FavoritesScreen'
import routes from './routes'
import colors from '../config/colors'

const Stack = createStackNavigator()

const FavoritesNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.charade
        },
        headerTitleStyle: {
          color: colors.white
        }
      }}
    >
      <Stack.Screen
        name={routes.FAVORITES_SCREEN}
        component={FavoritesScreen}
      />
    </Stack.Navigator>
  )
}

export default FavoritesNavigator
