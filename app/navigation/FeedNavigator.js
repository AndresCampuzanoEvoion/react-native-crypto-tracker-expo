import React from 'react'
import { createStackNavigator, HeaderBackground } from '@react-navigation/stack'

import CoinsScreen from '../screens/CoinsScreen'
import DetailsScreen from '../screens/DetailsScreen'
import routes from './routes'
import colors from '../config/colors'

const Stack = createStackNavigator()

const FeedNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.blackPearl
        },
        headerTitleStyle: {
          color: colors.white
        }
      }}
    >
      <Stack.Screen
        name={routes.COINS_SCREEN}
        component={CoinsScreen}
        options={{ title: 'Coins' }}
      />
      <Stack.Screen name={routes.DETAILS_SCREEN} component={DetailsScreen} />
    </Stack.Navigator>
  )
}

export default FeedNavigator
