export default Object.freeze({
  COINS_SCREEN: 'CoinsScreen',
  DETAILS_SCREEN: 'DetailsScreen',
  FAVORITES_SCREEN: 'FavoritesScreen'
})
