import React, { useEffect, useState } from 'react'
import { ActivityIndicator, FlatList } from 'react-native'
import coins from '../api/coins'
import CoinsItem from '../components/CoinsItem'
import ItemSeparator from '../components/ItemSeparator'
import routes from '../navigation/routes'

const CoinsScreen = ({ navigation }) => {
  const [coinsData, setCoinsData] = useState([])
  const [refreshing, setRefreshing] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    loadCoins()
  }, [])

  const handleRefresh = async () => {
    try {
      setRefreshing(true)
      let response = await fetch('https://api.coinlore.net/api/tickers/')
      let result = await response.json()
      setRefreshing(false)
      return setCoinsData(result)
    } catch (error) {
      setRefreshing(false)
      console.error('error: ', error)
    }
  }

  const loadCoins = async () => {
    try {
      setLoading(true)
      let response = await fetch('https://api.coinlore.net/api/tickers/')
      let result = await response.json()
      setLoading(false)
      return setCoinsData(result)
    } catch (error) {
      setLoading(false)
      console.error('error: ', error)
    }
  }

  return (
    <>
      {loading ? (
        <ActivityIndicator color='white' size='large' />
      ) : (
        <FlatList
          data={coinsData.data}
          keyExtractor={item => item.id.toString()}
          renderItem={({ item }) => (
            <CoinsItem
              name={item.name}
              symbol={item.symbol}
              price_usd={item.price_usd}
              percent_change_24h={item.percent_change_24h}
              onPress={() => navigation.navigate(routes.DETAILS_SCREEN, item)}
            />
          )}
          ItemSeparatorComponent={ItemSeparator}
          extraData={coinsData.data}
          refreshing={refreshing}
          onRefresh={handleRefresh}
        />
      )}
    </>
  )
}

export default CoinsScreen
