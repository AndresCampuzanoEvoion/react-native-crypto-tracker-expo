import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { ScreenContainer } from 'react-native-screens'

const DetailsScreen = ({ route }) => {
  const coin = route.params
  return (
    <ScreenContainer>
      <Text>{coin.name}</Text>
      <Text>{coin.symbol}</Text>
    </ScreenContainer>
  )
}

export default DetailsScreen

const styles = StyleSheet.create({})
