import React from 'react'
import { StyleSheet, Text } from 'react-native'
import ScreenComponent from '../components/ScreenComponent'

const FavoritesScreen = () => {
  return (
    <ScreenComponent>
      <Text>Favorites Screen here</Text>
    </ScreenComponent>
  )
}

export default FavoritesScreen

const styles = StyleSheet.create({})
